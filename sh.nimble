# Package
version       = "0.1.0"
author        = "Vegard Sjonfjell"
description   = "Python \"sh\" for nim"
license       = "ISC"
srcDir        = "src"

backend       = "cpp"

# Dependencies
requires "nim >= 1.0.6"
