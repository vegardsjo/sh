import sh
import strformat
import strutils
import os
import json

let shell = Shell(cwd: os.getHomeDir())

let params = commandLineParams()
if params.len < 1:
  stderr.writeLine fmt"Usage: {getAppFileName()} gist-id"
  quit(1)

let url = fmt"https://api.github.com/gists/{params[0]}"

for (fname, node) in shell.curl(silent=true, H="Accept: application/json",  url).parseJson()["files"].pairs:
  echo fmt"# {fname}"
  echo node["content"].getStr()
