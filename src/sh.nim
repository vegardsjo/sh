{.experimental: "dotOperators".}
import macros
import osproc
import streams
import strformat
import strutils

type Shell* =
  object
    cwd*: string

proc runShellCommand*(shell: Shell, command: string): string =
  let process = startProcess(command=command,
                             workingDir=shell.cwd,
                             # options={poStdErrToStdOut, poEvalCommand, poEchoCmd})
                             options={poStdErrToStdOut, poEvalCommand})

  # let (outputStream, errorStream) = (process.outputStream, process.errorStream)
  let pStream = process.outputStream
  discard process.waitForExit()
  let output = pStream.readAll()
  process.close()
  output

template `.`*(sh: Shell, field: untyped): string =
  runShellCommand(sh, astToStr(field))

proc optionPrefix(name: string): string =
  if name.len > 1: "--" else: "-"

macro `.()`*(shell: Shell, args: varargs[untyped]): untyped =
  let command = strVal(args[0])
  var params = newSeq[string]()

  proc option[T](name: string, value: T) =
    params.add &"{optionPrefix(name)}{name} {value}"

  proc option(name: string) =
    params.add &"{optionPrefix(name)}{name}"
    
  for node in args[1..^1]:
    case node.kind
    of nnkStrLit:
      params.add "\"" & node.strVal & "\""
    of nnkIdent:
      params.add "{" & node.strVal & "}"
    of nnkExprEqExpr:
      case node[1].kind:
        of nnkIdent:
          case node[1].strVal:
            of "true":
              option(node[0].strVal)
            of "false":
              discard
            else:
              option(node[0].strVal, "{" & node[1].strVal & "}")
        of nnkIntLit:
          option(node[0].strVal, node[1].intVal)
        of nnkStrLit:
          option(node[0].strVal, "\"" & node[1].strVal & "\"")
        else:
          error(&"Option value type {node[1].kind} not supported", node[1])
        
    else:
      error(&"Node with type {node.kind} not expected. Something funky with the parameter list?", node[1])

  nnkStmtList.newTree(
    nnkCall.newTree(
      bindSym"runShellCommand",
      shell,
      newCall(bindSym"fmt", newStrLitNode(command & " " & params.join(" ")))))
